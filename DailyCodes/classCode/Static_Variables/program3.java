

class Demo2{

	int x = 10;
	static int y  = 20;

	void fun1(){

		System.out.println(x);
		System.out.println(y);				//static and Non- static both variables can be acess from the non-static method


	}

	static void fun2(){

		System.out.println(y);
	}
}



class Client2{

	public static void main(String[] args){

		Demo2 obj1 = new Demo2();
		
		obj1.fun1();
		obj1.fun2();

		System.out.println(obj1.x);
		System.out.println(obj1.y);

	}
}
