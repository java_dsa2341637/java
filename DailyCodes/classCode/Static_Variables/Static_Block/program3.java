/*Before 1.6 version java developers used to write a code in static Block without main method.
 * Then java compulsory the main method from 1.6 version.
 * WE Cannot write the static variables inside any Block or method.
 * Static block is used to JDBC connection, Socket Connection establishment ,file open , etc for all actions like these must be redy before the main method.
 * These are the prerequisit for the set up.
 * If we write two static blocks then they merge in one.
 * We cannot inherit the static block in two differrent classes because it is limited for only that class.
 * We have to write main method after the 1.6 version of java
 */


class Demo3{

	static {

		System.out.println("Static Block1");

	}


	public static void main(String[] args){

		System.out.println("In Demo Main");

	}

}




class Client3{

		static {


		System.out.println("Static Block2");
		}

		public static void main(String[] args){



		System.out.println("In Client Main");

		}


		static {


		System.out.println("Static Block3");

		}

}



