/* We can access the static block without creating an object.
 * The static variable get intialise in the Static block at runtime.
 * Static Block come Earlier than that of main method
 *Static Block initialize the static variable.
 *When Bipush instruction in Bytecode get execute then the Static variable get initialize.
 *The getStatic() is used to access the static variables
 *
 */


class Demo {

	static {
		
		System.out.println("IN Static Block");

	}

	public static void main(String[] args){

		System.out.println("In main method");

	}
}
