/*We cannot write static variablr inside the static block  then it gives error: Illegal start of expression
 * If we create the object of the first class in another class then the Static block of the first class gets execute first.
 */


class Demo4{

	Demo4(){

		System.out.println("IN Constructor");
	}



	static {

		System.out.println("In static Block");

		}

}


class Client4{


	public static void main(String[] args){

		Demo4 obj = new Demo4();

		System.out.println("In main");

	}


}
