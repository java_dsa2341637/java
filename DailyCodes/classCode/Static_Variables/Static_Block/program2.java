

class Demo2{

	int x = 10;
	static int y = 20;

	static {

		System.out.println("In the Static Block1");

	}

	public static void main(String[] args){

		System.out.println("IN main method");

		Demo2 obj = new Demo2();
		System.out.println(obj.x);

	}

	static {

		System.out.println("In static Block2");
		System.out.println(y);


	}
}

	
