//Static variables are also known as the class variable
//There is special structure in Method Area which contain static block inside the special structure
//The Static block comes the very first than that of the main method in jvm
//When all the elements , methods are static then then there is one specail method is nonstatic which is constructor()
//static blocks can be directly acess by the class without creating the object



class StaticDemo{


	static int x = 10;
	static int y = 20;


	}


/*Bytecode of the code:
 * class StaticDemo {
  static int x;

  static int y;

  StaticDemo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  static {};
    Code:
       0: bipush        10
       2: putstatic     #2                  // Field x:I
       5: bipush        20
       7: putstatic     #3                  // Field y:I
      10: return
}
*/

