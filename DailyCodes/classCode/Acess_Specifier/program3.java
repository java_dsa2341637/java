//Real time Example
//In any class the instance variables are of that particular object
//IF we change in a particular instance variable of object1 then it will not Change in the object 2
//And if change the static variable , the change is common for all the objects

class Employee{

	int empId = 10;
	String empName = "Kanha";

	void empInfo(){
		
		System.out.println("ID = " + empId);
		System.out.println("Name = " + empName);

		}
}


class MainEmployee{

	public static void main(String[] args){

		Employee emp1 = new Employee();

		emp1.empInfo();

		System.out.println(emp1.empId);
		System.out.println(emp1.empName);

	}
}


