//Instance Variable are also called as object variable because the change in object1 instance variable is not affected in object
//We can change the static object without creating an onject ,  with the class.variable 


class Employee1{

	int empId = 10; 
	String name = "Kanha";

	static int y = 50;

	void empInfo(){

		System.out.println("ID = " + empId);		//Instance Variables 
		System.out.println("Name = " + name);		//INstance Variables
		System.out.println("y =" + y);			//Static variables are common for all the objects
		
	}
}


class MainEmployee1{

	public static void main(String[] args){

		Employee1 emp1 = new Employee1();
		Employee1 emp2 = new Employee1();

		emp1.empInfo();
		emp2.empInfo();

		System.out.println("----------------------");

		emp2.empId = 20;
		emp2.name = "Rahul";
		emp2.y = 5000;					//This change is applied to both emp1 and emp2

		emp1.empInfo();
		emp2.empInfo();


	}
}


