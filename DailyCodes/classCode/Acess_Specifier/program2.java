class Program2 {
	
	int x = 10;
	private int y = 20;

	void fun(){

		System.out.println(x);
		System.out.println(y);
	}
}


class Client2 {
	
	public static void main(String[] args){

	Program2 obj = new Program2();
	
	obj.fun();
	
	System.out.println(obj.x);
	//System.out.println(obj.y);		//error:program2.java:23: error: y has private access in Program2 
	//System.out.println(x);		//Cannot find Symbol
	//System.out.println(y);		//Cannot find Symbol
	}
}
	
	
		
