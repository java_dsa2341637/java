/*                         Acess Specifier in Java
 *There four types of acess Specifier in java
1: Default --- -We can acess Deafult Variable into same class, same file(Different class), same folder(Different files), But not in Different Folders
2: Private -----We can acess Private Variable only in the same Class
3: Public ------We can acess Public Variable in wherever we want to acess ; if we Acess it in the Differnt folder
4: Protected ---
*/

class Core2web{

	int numOfCourses = 8;
	private String favCourse = "Java";

	void display(){

		System.out.println("Number of Courses = " + numOfCourses);
		System.out.println("Favorite Course = " + favCourse);

	}
}

class Student{

	public static void main(String[] args){

		Core2web obj = new Core2web ();

		obj.display();

		System.out.println(obj.numOfCourses);
		
		//System.out.println(obj.favCourse); --- We cannot acess private variable in another class 
		//program1.java:31: error: favCourse has private access in Core2web


	}
}
