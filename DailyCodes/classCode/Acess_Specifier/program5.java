//Real Time Example of the static variables


class PetrolPump{

	int amount = 100; 
	String fuel = "Petrol";

	static int rate = 107;

	void fillPetrol(){

		System.out.println(amount + " rupees Fuel filled " );
		System.out.println("Fuel Type = " + fuel);
		System.out.println("Fuel Rate =" + rate);
		
	}
}


class MainPetrolPump{

	public static void main(String[] args){

		PetrolPump pp1 = new PetrolPump();
		PetrolPump pp2 = new PetrolPump();

		pp1.fillPetrol();
		pp2.fillPetrol();

		System.out.println("----------------------");

		pp2.amount = 200;
		pp2.fuel = "Disel";
		pp2.rate = 108;

		pp1.fillPetrol();
		pp2.fillPetrol();


	}
}


