//Only compile the code dont run 
//Compiler dose not need main method


class Core2web {

	int x = 10; 
	static int y =  10;

	void fun(){

		int z = 30;
	}
}


/*Bytecode :
 *
 * Compiled from "program4.java"
class Core2web {
  int x;

  static int y;

  Core2web();
    Code:
       0: aload_0
       1: invokespecial #1            // Method java/lang/Object."<init>":()V     //this call the constructor of parent class
       4: aload_0
       5: bipush        10 		// this stores the value of the variable
       7: putfield      #2            // Field x:I
      10: return

  void fun();
    Code:
       0: bipush        30
       2: istore_1
       3: return

  static {};
    Code:
       0: bipush        10
       2: putstatic     #3                  // Field y:I
       5: return
}
*/
