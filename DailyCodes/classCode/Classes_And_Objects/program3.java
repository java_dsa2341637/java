
class Cls3{

	int age = 10; 			//Non static / instance Variable / Global variable
	int jerNO =18;			//Non static / instance Variable / Global variable

	static void fun(){

		int x = 10;		//static variable
		System.out.println(x);
	}

	public static void main (String[] args){
		fun();
		
	}
}


/*Bytecode :
 * class Cls3 {
  int age;

  int jerNO;

  Cls3();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #2                  // Field age:I
      10: aload_0
      11: bipush        18
      13: putfield      #3                  // Field jerNO:I
      16: return

  static void fun();
    Code:
       0: bipush        10
       2: istore_0
       3: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
       6: iload_0
       7: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
      10: return

  public static void main(java.lang.String[]);
    Code:
       0: invokestatic  #6                  // Method fun:()V
       3: return
}

*/
