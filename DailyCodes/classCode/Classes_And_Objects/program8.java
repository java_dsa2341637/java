

class Cls5 {

	int x = 10;

	String str1 = "Shashi";				//On SCP

	void fun(){

		String str2 = "Shashi";			//ON SCP

		String str3 = new String("Core2web");	//ON HEAP

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);

	}

	public static void main(String[] args){

		Cls5 obj = new Cls5();

		obj.fun();
	}

}


