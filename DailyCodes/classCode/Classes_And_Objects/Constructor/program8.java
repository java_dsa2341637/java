//Setter method change the value throgh the constructor 
//getter method print the value

class Demo8{

	private int x = 10;				//in bytecode private variable is not declared as it is private for all except class members

	Demo8(){
	
		System.out.println(x);
	}

	Demo8(int x){
		
		this();
		System.out.println(x);
		System.out.println(this.x);

	}
}

class Client8{

	public static void main(String[] args){

		Demo8 obj = new Demo8(20);
	}

}



