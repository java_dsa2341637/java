/* this only works on the instance variables and methods
 * this is used to do call from one constructor to another constructor: that is also called as the "Constructor chaning"
 */

class Demo7{

	Demo7(){

		//this(50);				//error: recursive constructor invocation  : because  this statment call Demo7(int x)
							//it will execute in loop

		System.out.println("In Demo constructor");

	}

	Demo7(int x){

		this();					//this calls the Demo7() contructor beacause of the parameters
							//And this(), this  line be the first line in the constructor, if not it gives error
							//it call the constructor with same parameter
										
		System.out.println("In parameterized Constructor");

	}


	public static void main(String[] args){

		Demo7 obj =new Demo7(50);
	}
}

