/* When the call of construcor is execute from the another constructor then if we print the sop(this);
 * then we get the address of the object created by the class 
 */

class Player{

	private int jerNo =18;
	private String name = null;

	Player(){					//this goes as Player(Player this)

		System.out.println("In constructor");

	}

	void info(){

		System.out.println(jerNo + "=" + name);
	}
}

class Client9{

	public static void main (String[] args){
		Player obj = new Player();		//Player(obj) : call

		obj.info();				//info(obj) : call

	}
}




