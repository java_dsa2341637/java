

class Demo6{

	int x = 10;

	Demo6(){

		System.out.println("In No argument constructor");

	}

	Demo6(int x){

		System.out.println("In para Constructor");
		System.out.println(x);					//this prints the nearest (local variable) value
		System.out.println(this.x);				//'this' stores the address of an instance variable

	}

	public static void main(String [] args){
		Demo6 obj = new Demo6();
		Demo6 obj1 = new Demo6(20);

	}

}
