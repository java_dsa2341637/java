/* All instance variables and methods have this as the first hidden parameter
 *
 *
 *
 */

class Demo3{

	int x =10;

	Demo3(){

		System.out.println("In constructor");
	}

	void fun(){

		System.out.println(x);

	}

	public static void main(String[] args){

		Demo3 obj =new Demo3();

		obj.fun();						//fun(Demo this) or fun(obj)

	}

}

