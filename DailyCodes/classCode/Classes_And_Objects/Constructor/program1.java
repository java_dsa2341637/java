/* There are two types of constructors in the java:
 *
 * 1.Default Constructor: Added by Compiler
 * 2.No argument Constructor: written by programer
 *
 * Construcor are having same name as Class Name
 *After creating the object Constructor get implicit call from java
 * It have no return type
 * If programer have not written the constructor then at the time of compilation compiler adds the Default constructor
 * First call in constructor is to the involespecial()
 *
 *
 * Java Dosent have Destructor like in other language because there is Garbege Collector in the Java
 * Constructor is a special type of method in java which get implicit call by java
 * Java is robust language
 */



class Demo{

	int x = 10;

	Demo(){								//Demo(Demo this)

		System.out.println("IN Constructor");
		System.out.println(this);
		System.out.println(this.x);
	}


	void fun(){							//fun(Demo this)

		System.out.println(this);
		System.out.println(x);

	}


	public static void main(String[] args){

		Demo  obj = new Demo();					//Demo(obj)
									
		
		System.out.println(obj);
		obj.fun();						//fun(obj1)
	}

}

