/*from constructor "this" can change private instance variable from the another class
 * When string is initilize then the String goed on SCP
 * The Setter method can be defined as the instance method which change  the data in the private instance  method or variable.
 * The getter method is method which prints . it is also a instance method
 */


class Player1{
	private int jerNo =0;
	private String name = null;

	Player1(int jerNo , String name ){		//Player1(Player1 this, int jerNo , String name)
	
		this.jerNo = jerNo;
		this.name = name;

		System.out.println("In constructor");

	}

	void info(){

		System.out.println(jerNo + "=" + name);

	}
}

class Client10{

	public static void main(String[] args){

		Player1 obj = new Player1(18, "Virat");			//Player1(obj , 18, Virat)

		obj.info();						//info(obj)
		
		Player1 obj1 = new Player1(7 ,"MSD");			//Player1(obj1 , 7, MSD)
		obj1.info();						//info(obj1)

		Player1 obj2 = new Player1(45 ,"Rohit Sharma");		//Player1(obj2, 45 , Rohit Sharma)
		obj2.info();						//info(obj2)
			
	}
}
