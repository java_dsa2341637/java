/* We cannot write same type of constructor twice in the class because of ,
 * Method Signiture:  This concept stores the method name and its datatype
 * eg, Demo(int x){}   it stores as the Demo(int)
 * fun(float z) this get stored as fun(float)
 * In this type this stores every method and there datatype
 * compiler check the method signiture for each method that , that type of method signiture is available before or not .
 * if method signiture is alredy present then compiler gives error that method is alredy defined in the class
 */

class Demo5{
	 
	int x = 10;
	
	Demo5(){

	System.out.println("In constructor");
	System.out.println("x =" + x);
	}


	Demo5(){

	}

}

		
