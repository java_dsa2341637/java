/* When we create an object
 * Demo obj = new Demo();
 * obj.method()
 * the internal call goes like method(obj)
 * And constructor as Demo(Demo obj)/Demo(Demo this)
 * "this " is called as the hidden pointer in the java
 * compiler add this hidden pointer/ reference in the code
 
 * this helps in the intialize the instatnce variables 
 * in all the constructors "this" is the the first hidden parameter
 * this only works on the non-static variables and methods
 */



class Demo2{

	int x = 10;

	Demo2(){

		int x = 20;

		System.out.println(x);				//this statment prints the nearest valuve (Local variable valuve)
		System.out.println(this.x);			//this statment prints the the instance variable because this stores address of an object
		

	}

	public static void main (String[] args){

		Demo2 obj = new Demo2();

	}
}
