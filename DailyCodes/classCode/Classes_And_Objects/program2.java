//The non static variable get place inside the constructor of a class eg. cls() So our int y is will get stored in constructor
//When we create an object of a class then the constructor of our class gets the place on the Heap Secssion. 
//After creating an object the non-static variable y will get actully placed inside the Heap
//Therefore if we need to acess the non - static variable y we can acess it by creating an object of class 


class Cls2{
	
	int y = 20; 


	static void fun(){
		int z= 30;
		System.out.println(z);

	}

	public static void main(String[] args){

		int x = 10;			
		System.out.println(x);				//1 step
		
		fun ();						//2 step 
		
		//System.out.println(y);  
		//program2.java:25: error: non-static variable y cannot be referenced from a static context
		//--- WE cannot access the non-static variable from static block . For accessing a non
		//static varible we need to create a object of class. Then we can access the Variable by
		// System.out.println(obj.y)
							
	}

}




/*Bytecode :
 *
 * class Cls {
  int y;

  Cls();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        20
       7: putfield      #2                  // Field y:I
      10: return

  static void fun();
    Code:
       0: bipush        30
       2: istore_0
       3: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
       6: iload_0
       7: invokevirtual #4                  // Method java/io/PrintStream.println:(I)V
      10: return

  public static void main(java.lang.String[]);
    Code:
       0: bipush        10
       2: istore_1
       3: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
       6: iload_1
       7: invokevirtual #4                  // Method java/io/PrintStream.println:(I)V
      10: invokestatic  #5                  // Method fun:()V
      13: return
}
*/

