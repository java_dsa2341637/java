//OOP
//Class contains :- Variables , Methods , Constructor, Static block.
//Constructor is special method .
//There are Three types of method Static , non-static variables (Global variables or instance variables) and Local veriables .
// We can access the non Static variables and Methods by 1- Making that variable or Method Static using static keyword 2- Creating an Object and then call the method or variable


class Clsobj1 {

		int y= 20;					//Instance or Global Or Non-static variable

		void gun(){
			System.out.println("This is gun method");
		}

		static void fun(){
			int z = 30;
			System.out.println(z);
			Clsobj1 obj = new Clsobj1();		//Here we create an object of Clsobj class for accessing gun me									 -thod through the object.
			obj.gun();				//we can acess the method by giving the call.
	}

		public static void main(String[] args){
			int x = 10;
			System.out.println(x);
			fun();
		
		}	
	}

