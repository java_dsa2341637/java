/* Instance variable (Object):
 * There types three types of instance:
 * 1. instance variable
 * 2.Instance BLOCK
 * 3. Instance method:
 * a. Constructor 
 * b. non-static method()
 *
 * Insatnce variables are also called as the global non static 
 * Non static method are initilize in the constructor.
 * Instance block is the part of the constructor.
 * We can access any variables inside the Non static method
 *
 */


class Demo{

	int x = 10;

	Demo(){

		System.out.println("In Constructor");

	}

	

	{										//Instance Block
		System.out.println("Instance Block1");

	}



	public static void main(String[] args){

		Demo obj = new Demo();

		System.out.println("In main");
	}


	{

		System.out.println("INstance Block2");

	}

}







