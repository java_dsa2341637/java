/*                     At the time of Execution the following sequence is followed by JVM
 *
 *
 *                     1. STATIC VARIABLES
 *
 *                     2.STATIC BLOCKS
 *
 *                     3.STATIC METHODS()
 *
 *                     4.INSATNCE VARIABLE
 *
 *                     5.INSTANCE BLOCK
 *
 *                     6.CONSTRUCTOR
 *
 *                     7.INSTANCE METHODS()
 *
 */



class SequenceDemo{

	int x = 10;

	static int y = 20;

	SequenceDemo(){

		System.out.println("In constructor");

	}

	static {

		System.out.println("In static block 1");

	}


	{

		System.out.println("In instance Block 1");

	}

	public static void main(String[] args){


		System.out.println("In main");

		SequenceDemo obj = new SequenceDemo();

	}


	static {



		System.out.println("IN static Block2");

	}


	{
		System.out.println("In instance Block 2");

	}
}







