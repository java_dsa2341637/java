//In java intial capacity is for user to give the maximum capacity of string object for storing the characters or string in it.
//This initial capacity give capacity to object at time of creation of object.
//After the initial capacity is full then java increase the capacity.


class CapacityDemo1{

	public static void main(String[] args){
		StringBuffer sb = new StringBuffer(100);			//initial capacity
		System.out.println(sb.capacity());

		sb.append("Binecaps");

		sb.append("Core2web");
		System.out.println(sb);
		System.out.println(sb.capacity());

		sb.append("Incubator");
		System.out.println(sb);
		System.out.println(sb.capacity());


	}
}


