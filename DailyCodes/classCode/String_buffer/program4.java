
//In string Class we cannot change the string because the string class is muttable(Non changeble).
//In StringBuffer class we can change the string because the stringBuffer class id muttable (Changeble).
//For making change in string Java introduce a new class Called as StringBuffer 
//StrringBuffer class objects are directly created on Heap secssion.
//Only string class object can be stored on SCP




class StringBufferDemo1 {

	public static void main(String[] args){
	
		StringBuffer sb = new StringBuffer ("Onkar");		//always write StringBuffer class with creating a new object by new keyword
		System.out.println(System.identityHashCode(sb));	//225534817
		System.out.println(sb);

		sb.append("Inamdar");					//append method in StringBuffer class is same as the concat() in String class
									//this method Join two strings and and also store the change in original string
		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));	//225534817  Same identityHashCode getting Because string Buffer class is muttable)
									//String Buffer class can accept change in it and stores change 
									//Therefore we are getting Same identityHsahCode

	}
}

