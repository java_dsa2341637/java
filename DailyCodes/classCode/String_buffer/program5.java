/* In string BUffer class When we create any new string without any input value we for that we get 16 character capacity by default
 * If we pass the string value at the tim eof creation of string then we get Capcity by following formula
 * Capacity = Total number of characters + 16
 * Example: StringBuffer str1 = new StringBuffer ();Then we get 16 capacity		
   But	StringBuffer str1 = new StringBuffer ("Onkar");Then we get Capacity = 5+16 = 21
*/   




class StringBufferDemo2 {

	public static void main(String[] args){
	
		StringBuffer str1 = new StringBuffer ();
		System.out.println(str1);			// It print blank space  because there is nothing String
		System.out.println(str1.capacity());		// This method gives the capacity		
	
		
		str1.append("Onkar");				
		System.out.println(str1);

		System.out.println(str1.capacity());		
	

	}
}

