/*Reverse();
 * Reverse the whole string 
 */

class ReverseDemo{
	public static void main(String[] args) {

		StringBuffer sb1 = new StringBuffer ("Onkar");
		
		System.out.println(sb1.reverse());

		String str1 = "Core2web";

		StringBuffer sb2 = new StringBuffer(str1);
		
		//Str1 = sb2.reverse();
		//error:cannot find symbol: bacause reverse is StringBuffer class method

		str1 = sb2.reverse().toString();			//This is called as method Chaning.
									//In this first sb2.reverse method reverse the String and 
									//Seconly the reversed string is converted to string using toString method and store 
									//in str1

		System.out.println(str1);
	}
}

