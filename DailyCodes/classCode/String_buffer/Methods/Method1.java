/*append();
 * joins two strings and also store in original string
 * parameters: String(str the <code> String </code> to append
 */


class AppendDemo{
	public static void main (String[] args){

		StringBuffer sb = new StringBuffer("Onkar");
		System.out.println(sb);	
		String str = "Inamdar";
		System.out.println(str);	

		sb.append(str);
		System.out.println(sb);	
		System.out.println(str);
	}
}	
