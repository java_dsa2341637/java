/*Delete();
 * This method deletes the substring from the given string
 * Parameters : sb.delete(Starting index , end String Index);
 */

class DeleteDemo{
	public static void main(String[] args){
		StringBuffer sb = new StringBuffer ("OnkarInamdar");		//here 2nd index character is k and 9th index character is d
										//when we execute the code we get answer as Ondar beacause java delete the
										//string till previous index number character	
		sb.delete(2,9);
		System.out.println(sb);						//Ondar

	}
}
