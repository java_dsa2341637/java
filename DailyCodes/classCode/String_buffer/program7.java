//When inserting the string into the object the object capacity gets full then java itself increase the size by creating new object and copy all the previous data to new new string object
//New capacity = (current Capacity +1) *2

class StringBufferDemo4 {
	public static void main(String[] args){

		StringBuffer str = new StringBuffer ();
		System.out.println(str.capacity());				//16
		System.out.println(str);					
		
		str.append("Shashi");
		System.out.println(str.capacity());				//16
		System.out.println(str);

		str.append("Bagal");	
		System.out.println(str.capacity());				//16	
		System.out.println(str);
		
		str.append("Core2web");
		System.out.println(str.capacity());				//34
		System.out.println(str);
	}


}





