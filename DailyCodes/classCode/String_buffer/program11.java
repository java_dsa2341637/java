//Concat mathod in String class and append method of StringBuffer class are differnt
//Cncat method joins the two string and dose not store the updated string in original string object because strings in  string class are immutable
//The append method in stringBuffer class joins the two strings and it also stores the updated String in the original string also becuse strings in StringBuffer class are muttable


class SbDemo2 {
	public static void main(String[] args){

		String str1 = "Shashi";
		String str2 = "Bagal";

		StringBuffer str3 = new StringBuffer ("Core2web");

		str1.concat(str2);
		str3.append(str2);

		System.out.println(str1);					//Shashi
		System.out.println(str2);					//Bagal
		System.out.println(str3);					//Core2webBagal
	}
}
