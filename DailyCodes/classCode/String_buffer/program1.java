// In conact() method we can join two strings



class ConcatDemo1 {
	public static void main (String[] args){

		String str1 = "Onkar"; //The new object is get create on String Constant pool  (SCP)
		String str2 = str1.concat("Inamdar");// This method join Onkar and Inamdar = OnkarInamdar
		
		System.out.println(str1);
		System.out.println(str2);

	}
}
