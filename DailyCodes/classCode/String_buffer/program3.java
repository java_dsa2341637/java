
class ConcatDemo3 {

	public static void main(String[] args){

		String str1 = "Shashi";
		
		System.out.println(System.identityHashCode(str1));
		
		str1 = str1.concat("Bagal");		// It internally creates new object On heap and store the new string in it

		System.out.println(str1);
	
        	System.out.println(System.identityHashCode(str1));
	
	}
}
