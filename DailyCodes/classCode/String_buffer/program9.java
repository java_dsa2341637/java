//We cannot join 
//String class object to any string by using append() method because the append method is of StringBuffer class
//String str4 = str3.append(str1); output =  error: incompatible types: StringBuffer cannot be converted to String
// because we join the the two strings by using the append method and we put it in the string class object

		

class CapacityDemo2{

	public static void main(String[] args){

		String str1 = "Onkar";
		String str2 = new String("Inamdar");

		StringBuffer str3 = new StringBuffer ("Core2web");
		StringBuffer str4 = str3.append(str1);

		System.out.println(str1);			//Onkar
		System.out.println(str2);			//Inamdar
		System.out.println(str3);			//Core2web then change to Core2webOnkar
		System.out.println(str4);			//Core2webOnkar
	}
}
