//If we pass the value to string then the capacity will be Number of character + 16 
//capacity(); gives the maximun capacity of the String object

class StringBufferDemo3 {
	public static void main(String[] args){

		StringBuffer str1 = new StringBuffer("Onkar");
		System.out.println(str1.capacity());			//21= 5(Onkar) +16

		StringBuffer str2 = new StringBuffer ("Inamdar");	//capacity (); shows the capacity of the string object
		System.out.println(str1.capacity());		
	}
}
